package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

/**
 * Classe per il test della classe Proxy
 * @author mario
 *
 */
public class Client {

	public static void main(String[] args) {
		RandomAccessFile file;
		try {
			file = new RandomAccessFile(new File("jsonFile"), "r");
			String line="";
			while(file.getFilePointer()!=file.length()){
				line=file.readLine();
				//System.out.println(line);
			}			
			JSONObject jsonObj = new JSONObject();
			jsonObj = (JSONObject) JSONSerializer.toJSON(line); 
			Proxy proxy=new Proxy();
			String urlFile=proxy.installNetwork(jsonObj);
			System.out.println("L'url del file contenente i risultati è il seguente: "+
			urlFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

	}

}
