package server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.Network;

/**
 * Servlet implementation class MyHttpServlet
 */
@WebServlet("/MyHttpServlet")
public class MyHttpServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyHttpServlet() {
        super();
        this.network=new Network();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//String req=(String)request.getAttribute("request");		
		System.out.println("richiesta ricevuta");
		String urlFile=network.installNetwork((String)request.getParameter("platform"), (String)request.getParameter("security"),
				(String)request.getParameter("key"),(String)request.getParameter("sign"),(String)request.getParameter("cipher"),
				(String)request.getParameter("samplePeriod"), (String)request.getParameter("sampleDuration"), 
				(String)request.getParameter("measurementCode"));		
		response.getOutputStream().println(urlFile);				
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String req=request.getParameter("req");
		System.out.println("richiesta ricevuta");
	}
	
	//attributi privati
	private Network network;

}
